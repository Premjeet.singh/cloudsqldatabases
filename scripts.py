######## Program Begins Here ########

#### Import Python libraries ####

# import sqlite3
# from google.cloud import storage
# import pandas as pd
# import numpy as np
# from datetime import datetime
import mysql.connector
import sys

#### Establish Connetion ####

cnx = mysql.connector.connect(user='root',
                              host='35.200.144.218',
                              database='tests')

cursor = cnx.cursor()

#### Connetion Established ####

#### Execute query ####

sql ='''CREATE TABLE EMPLOYEE(
   FIRST_NAME CHAR(20) NOT NULL,
   LAST_NAME CHAR(20),
   AGE INT,
   SEX CHAR(1),
   INCOME FLOAT
)'''
cursor.execute("Show tables;")

myresult = cursor.fetchall()

for x in myresult:
    print(x)
cursor.execute(sql)

#### Extremely Important: Close your SQL connection ####

cursor.close()
cnx.close()

######## Program Ends Here ########